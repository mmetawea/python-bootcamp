import json
import os

from bs4 import BeautifulSoup
import locale
locale.getdefaultlocale()


# for i in os.chdir("C:\\Users\\Mohammed\\gitlab\\Complete-Python-3-Bootcamp\\00-Python Object and Data Structure Basics\\)
fname = "D:\\home\\GitLibrary\\Python_bootcamp\\02-Python Statements\\03-for Loops.ipynb"
# ofname = "C:\\Users\\Mohammed\\gitlab\\Complete-Python-3-Bootcamp\\00-Python Object and Data Structure Basics\\06-Tuples.py"

with open(fname, "r") as read_file:
    json_string = json.load(read_file)

cont = json_string.json()
print(cont)

import json
json_string.dump()


""" ipython nbconvert *.ipynb"""
# text = 'Topic'
# data = json.loads(json_string)
data = json_string

for i in range(0, len(data['cells'])):
    print(data['cells'][i]['source'])
    # string.join(text)
    # string = str (data['cells'][i]['source'])
    # string.join(text)
    # text = data['cells'][i]['source']
    # print(text)

# """clean special chars and extra whitespace"""
# input_str.Message = re.sub("\W", "", input_str.Message).strip()
# sub(​pattern, repl, string​)
# print re.sub(r"[ab]", "z", "abcABC", flags=re.IGNORECASE)
# string = string.str.lower()
# string = string.strip(' ')
# mystring = string.replace("\n', '\n', ", "")
# string = string.split()
# Striping (non-relevant) punctuation
# input_str.Message.translate(str.maketrans("", "", ",.-'\"():;+/?$°@"))
# Stripping accents
# input_str = ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')

# print(string)
