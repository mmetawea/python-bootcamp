"""Multiprocessing Example: Monte Carlo

Let's code out an example to see how the parts fit together. We can time our results using the timeit module to measure any performance gains. Our task is to apply the Monte Carlo Method to estimate the value of Pi.

If you draw a circle of radius 1 (a unit circle) and enclose it in a square, the areas of the two shapes are given as
Area Formulas circle
πr2
square
4r2

Therefore, the ratio of the volume of the circle to the volume of the square is
π4

The Monte Carlo Method plots a series of random points inside the square. By comparing the number that fall within the circle to those that fall outside, with a large enough sample we should have a good approximation of Pi. """

from random import random  # perform this import outside the function

def find_pi(n):
    """
    Function to estimate the value of Pi
    """
    inside=0

    for i in range(0,n):
        x=random()
        y=random()
        if (x*x+y*y)**(0.5)<=1:  # if i falls inside the circle
            inside+=1

    pi=4*inside/n
    print((n, pi))
    # return pi

def find_pi_recur(n):
    """
    Function to estimate the value of Pi
    """
    inside=0
    if n > 1:
        find_pi(n-1)
    x=random()
    y=random()
    if (x*x+y*y)**(0.5)<=1:  # if i falls inside the circle
        inside+=1

    pi=4*inside/n
    # print((n, pi))
    return pi


for x in range(1000,3000):
    find_pi(x)

find_pi_recur(5)
find_pi(15000)
22/7 == 3.142857142857143
