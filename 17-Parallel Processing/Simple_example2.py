
#Example2.py
'''
A more realistic thread pool example
'''

import time
import threading
import queue as Queue
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class Consumer(threading.Thread):
  def __init__(self, queue):
    threading.Thread.__init__(self)
    self._queue = queue

  def run(self):
    while True:
      content = self._queue.get()
      if isinstance(content, str) and content == 'quit':
        break
      # response = urllib2.urlopen(content)
      http = urllib3.PoolManager(num_pools=200)
      response = http.request('GET', content)
    print ('Bye byes!')


def Producer():
  urls = [
    'http://www.python.org', 'https://www.yahoo.com',
    'https://www.scala-lang.org/', 'https://www.google.com'
    # etc..
  ]
  queue = Queue.Queue()
  worker_threads = build_worker_pool(queue, 4)
  start_time = time.time()

  # Add the urls to process
  for url in urls:
    queue.put(url)
  # Add the poison pillv
  for worker in worker_threads:
    queue.put('quit')
  for worker in worker_threads:
    worker.join()

  print ('Done! Time taken: {}'.format(time.time() - start_time))

def build_worker_pool(queue, size):
  workers = []
  for _ in range(size):
    worker = Consumer(queue)
    worker.start()
    workers.append(worker)
  return workers

if __name__ == '__main__':
  Producer()
