import threading
import datetime


class ThreadClass(threading.Thread):
    def run(self):
        now = datetime.datetime.now()
        print("%s says Hello World at time: %s" % (self.getName(), now))


for i in range(5):
    t = ThreadClass()
    t.start()


# print( '%s %s %s'%('python','is','fun'))
