"""Fortunately, we've already learned one of the most valuable tools we'll need – the map() function. When we apply it using two standard libraries, multiprocessing and multiprocessing.dummy, setting up parallel processes and threads becomes fairly straightforward.

Here's a classic multithreading example provided by IBM and adapted by Chris Kiehl where you divide the task of retrieving web pages across multiple threads:"""

import time
import threading
import queue as thqueue
import urllib3

#Introucing MAP..

class Consumer(threading.Thread):
    def __init__(self, queue):
        threading.Thread.__init__(self)
        self._queue = queue

    def run(self):
        while True:
            content = self._queue.get()
            if isinstance(content, str) and content == 'quit':
                break
            # response = urllib3.urlopen(content)
            http = urllib3.PoolManager(num_pools=150)
            # http = urllib3.HTTPConnectionPool() # 'google.com', maxsize=10, block=True)
            response = http.request('GET', content)

        print('Thanks!')


def Producer():
    urls = [
        'http://www.python.org', 'http://www.yahoo.com', 'http://www.google.com'
        # etc..
        ]
    queue = thqueue.Queue()
    worker_threads = build_worker_pool(queue, 8)
    start_time = time.time()

    # Add the urls to process
    for url in urls:
        queue.put(url)
    # Add the poison pill
    for worker in worker_threads:
        queue.put('quit')
    for worker in worker_threads:
        worker.join()

    print('Done! Time taken: {}'.format(time.time() - start_time))


def build_worker_pool(queue, size):
    workers = []
    for _ in range(size):
        worker = Consumer(queue)
        worker.start()
        workers.append(worker)
    return workers


if __name__ == '__main__':
    Producer()
