""" Pyhton Applet to block some sites during working hours"""
# Windows Execution Premissions # https://stackoverflow.com/questions/36434764/permissionerror-errno-13-permission-denied
# Windows Service - running Applet as a Service #
# https://stackoverflow.com/questions/32404/how-do-you-run-a-python-script-as-a-service-in-windows

import time
from datetime import datetime as dt

# hosts_temp=r"D:\Dropbox\pp\block_websites\Demo\hosts"
# hosts_path="/etc/hosts"                               #Mac & Linux
hosts_path="C:\Windows\System32\drivers\etc\hosts"      #Windows
redirect="127.0.0.1"
website_list=["www.facebook.com","facebook.com","www.gmail.com","www.outlook.com"]

def timeCheck():
    return dt(dt.now().year,dt.now().month,dt.now().day,8) < dt.now() < dt(dt.now().year,dt.now().month,dt.now().day,16)

while True:
    if timeCheck():
        print("Working hours...")
        with open(hosts_path,'r+') as file:
            content=file.read()
            for website in website_list:
                if website in content:
                    pass
                else:
                    file.write(redirect+" "+ website+"\n")
    else:
        with open(hosts_path,'r+') as file:
            content=file.readlines()
            # print(content)
            file.seek(0)
            for line in content:
                # print(line)
                if not any(website in line for website in website_list):
                    file.write(line)
            file.truncate()
        print("Fun hours...")
    time.sleep(5)
