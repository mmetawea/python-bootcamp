# Read https://realpython.com/python-json/
import difflib
import json
import os

# dictfile = "C:\\Users\\Mohammed\\gitlab\\Py3-Bootcamp\\19-Mega10_apps\\App1-dictionary\\dictionary.py"
# dictfile = "D:\\home\\GitLibrary\\\Python_bootcamp\\19-Mega10_apps\\App1-dictionary\\dictionary.py"
os.chdir("D:\\home\\GitLibrary\\\Python_bootcamp\\19-Mega10_apps\\App1-dictionary")
# with open("D:\\home\\GitLibrary\\\Python_bootcamp\\19-Mega10_apps\\App1-dictionary\\dictionary.py", "r") as read_file:
with open('data.json', "r") as read_file:
    dictbase = json.load(read_file)

# for open('data.json', mode='r') as f:
#   dictionary = f.read()


def translate(word):
    word = word.lower()
    if word in dictbase.keys():
        return dictbase[word]
    elif word.title() in dictbase.keys():
        return dictbase[word.title()]
    elif word.upper() in dictbase.keys():
        return dictbase[word.upper()]
    else:
        corrected = fuzzmatch(word)
        if not corrected:
            print('This is an invalid word, please check the spelling')
            quit()
        else:
            print(
                f"We couldn't find a match, perhaps you meant '{corrected}' which means:")
            yn = input().upper()
            if yn == "Y":
                return dictbase[corrected]
            elif yn == "N":
                return "The word doesn't exist. Please double check it."
            else:
                return "We didn't understand your entry."


def fuzzmatch(word):
    w = (difflib.get_close_matches(word, dictbase.keys(), n=1, cutoff=0.80))
    if w:
        return w[0]
    else:
        return None


if __name__ == "__main__":
    wordLookup = input("Enter the word you would like to translate: ")
    result = translate(wordLookup)
    if result:
        print(result)
