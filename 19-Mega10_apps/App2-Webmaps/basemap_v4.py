# Map built by https://leafletjs.com/
# http://python-visualization.github.io/folium/docs-v0.5.0/quickstart.html
# folium icons is from leaflet https://github.com/lvoogdt/Leaflet.awesome-markers
# http://nbviewer.jupyter.org/github/python-visualization/folium/tree/master/examples/
# https://www.kaggle.com/rachan/how-to-folium-for-maps-heatmaps-time-analysis
##############################
## Adding Polygon layer

import folium
import pandas

data = pandas.read_csv("Volcanoes.txt")
lat = list(data["LAT"])
lon = list(data["LON"])
elev = list(data["ELEV"])
name = list(data["NAME"])

html = """
<h4>Volcano information:</h4>
<a href="https://www.google.com/search?q=%%22%s%%22" target="_blank">%s</a>
Height: %s m
"""

def color_producer(elv):
    # print(type(elv))
    # data["ELEV"].describe()
    if elv < 2000:
        return 'green'
    elif 2000 <= elv <= 3000:
        return 'orange'
    else:
        return 'red'

map = folium.Map(location=[38.58, -99.09], zoom_start=5, tiles="Mapbox Bright")
fg = folium.FeatureGroup(name = "My Map")

for lt, ln, el, name in zip(lat, lon, elev, name):
    iframe = folium.IFrame(html=html % (name, name, el), width=200, height=100)
    # fg.add_child(folium.Marker(location=[lt, ln], popup=folium.Popup(iframe), icon = folium.Icon(color = color_producer(el))))
    fg.add_child(folium.CircleMarker(location=[lt, ln],radius=7, popup=folium.Popup(iframe),  color = color_producer(el), fill_color= color_producer(el), fill_opacity=0.7, fill=False ))

fg.add_child(folium.GeoJson(data=(open('world.json', 'r', encoding='utf-8-sig').read())))
# help(folium.GeoJson)

map.add_child(fg)
map.save("Map_html_popup_advanced.html")

# dir(folium.Icon)
# help(folium.Icon)
