import pandas as pd
# nom setup
from geopy.geocoders import Nominatim
nom = Nominatim(user_agent="my-geocod-app")

#reading file
df = pd.read_csv('supermarkets.csv')

# Nom try
# nom.geocode("3995 23rd St, San Francisco, CA 94114")
# Location(3995, 23rd Street, Noe Valley, SF, California, 94114, USA, (37.7529648, -122.4317141, 0.0))

# concat Adresses
df['Address'] = df['Address']+", "+ df['City']+', '+ df['State']+' '+ df['Country']

# df['Coordinates'][0].latitude
# df['Coordinates'][0].longitude

""" Returning a Lat,Lang tuple
df['Coordinates'] = df['Address'].apply(nom.geocode)
df['Coordinates'] = df['Coordinates'].apply(lambda x: (x.latitude, x.longitude) if x != None else None)
"""

# returing Lat & Lang columns
df['Coordinates'] = df['Address'].apply(nom.geocode)
df['Latitude'] = df['Coordinates'].apply(lambda x: x.latitude if x != None else None)
df['Longitude'] = df['Coordinates'].apply(lambda x: x.longitude if x != None else None)

# df_coord = df['Latitude'], df['Longitude']
dfCoord = df[['ID', 'Name', 'Employees','Latitude' ,'Longitude', 'Country']]
