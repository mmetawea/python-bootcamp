from django.urls import path

from . import views

app_name = 'polls'
urlpatterns = [

    # Old Views, refactoring..
    # # ex: /polls/
    # path('', views.index, name='index'),
    #
    # # ex: /polls/5/
    # # the 'name' value as called by the {% url %} template tag
    # path('<int:question_id>/', views.detail, name = 'detail'),
    #
    # # If you want to change the URL of the polls detail view to something else, perhaps to something like polls/specifics/12/ instead of doing it in the template (or templates) you would change it in polls/urls.py:
    # path('specifics/<int:question_id>/', views.detail, name='detail'),
    #
    # # ex: /polls/5/results/
    # path('<int:question_id>/results/', views.results, name='results'),
    #
    # # ex: /polls/5/vote/
    # path('<int:question_id>/vote/', views.vote, name='vote')

    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
]
