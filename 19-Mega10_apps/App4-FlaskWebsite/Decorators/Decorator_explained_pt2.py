# https://gist.github.com/Zearin/2f40b7b9cfc51132851a

"""Handcrafted decorators"""

# A decorator is a function that expects ANOTHER function as parameter
def my_shiny_new_decorator(a_function_to_decorate):

    # Inside, the decorator defines a function on the fly: the wrapper.
    # This function is going to be wrapped around the original function
    # so it can execute code before and after it.
    def the_wrapper_around_the_original_function():

        # Put here the code you want to be executed BEFORE the original
        # function is called
        print( 'Before the function runs')

        # Call the function here (using parentheses)
        a_function_to_decorate()

        # Put here the code you want to be executed AFTER the original
        # function is called
        print( 'After the function runs')

    # At this point, `a_function_to_decorate` HAS NEVER BEEN EXECUTED.
    # We return the wrapper function we have just created.
    # The wrapper contains the function and the code to execute before
    # and after. It’s ready to use!
    return the_wrapper_around_the_original_function

# Now imagine you create a function you don’t want to ever touch again.
@my_shiny_new_decorator
def a_stand_alone_function():
    print( 'I am a stand alone function, don’t you dare modify me')

# a_stand_alone_function()
#outputs: I am a stand alone function, don't you dare modify me

# Well, you can decorate it to extend its behavior.
# Just pass it to the decorator, it will wrap it dynamically in
# any code you want and return you a new function ready to be used:

"""a_stand_alone_function_decorated = my_shiny_new_decorator(a_stand_alone_function)"""
a_stand_alone_function_decorated()
#outputs:
#Before the function runs
#I am a stand alone function, don't you dare modify me
#After the function runs


"""Decorators are just a pythonic variant of the decorator design pattern. There are several classic design patterns embedded in Python to ease development (like iterators).
Of course, you can accumulate decorators:"""


def bread(func):
    def wrapper():
        print( "</''''''\>")
        func()
        print( "<\______/>")
    return wrapper

def ingredients(func):
    def wrapper():
        print( '#tomatoes#')
        func()
        print( '~salad~')
    return wrapper

def sandwich(food='--ham--'):
    print( food)

sandwich()
#outputs: --ham--
sandwich = bread(ingredients(sandwich))
sandwich()
#outputs:
#</''''''\>
# #tomatoes#
# --ham--
# ~salad~
#<\______/>


@bread
@ingredients
def sandwich(food='--ham--'):
    print( food)

sandwich()

# The decorator to make it bold
def makebold(fn):
    # The new function the decorator returns
    def wrapper():
        # Insertion of some code before and after
        return '<b>' + fn() + '</b>'
    return wrapper

# The decorator to make it italic
def makeitalic(fn):
    # The new function the decorator returns
    def wrapper():
        # Insertion of some code before and after
        return '<i>' + fn() + '</i>'
    return wrapper

@makebold
@makeitalic
def say():
    return 'hello'

print( say())
#outputs: <b><i>hello</i></b>

# This is the exact equivalent to
def say():
    return 'hello'
say = makebold(makeitalic(say))

print( say() )
#outputs: <b><i>hello</i></b>
