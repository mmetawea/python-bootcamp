# https://gist.github.com/Zearin/2f40b7b9cfc51132851a

"""Taking decorators to the next level
Passing arguments to the decorated function"""

# It’s not black magic, you just have to let the wrapper, pass the argument:
def a_decorator_passing_arguments(function_to_decorate):
    def a_wrapper_accepting_arguments(arg1, arg2):
        print( 'I got args! Look:', arg1, arg2)
        function_to_decorate(arg1, arg2)
    return a_wrapper_accepting_arguments

def a_decorator(function_to_decorate):
    def a_wrapper():
        # print( 'I got args! Look:', arg1, arg2)
        function_to_decorate()
    return a_wrapper


# Since when you are calling the function returned by the decorator, you are calling the wrapper, passing arguments to the wrapper will let it pass them to the decorated function
@a_decorator_passing_arguments
def print_full_name(first_name, last_name):
    print( 'My name is', first_name, last_name)

print_full_name('Peter', 'Venkman')


"""Decorating methods

One nifty thing about Python is that methods and functions are really the same. The only difference is that methods expect that their first argument is a reference to the current object (self).
That means you can build a decorator for methods the same way! Just remember to take self into consideration:"""

def method_friendly_decorator(method_to_decorate):
    def wrapper(self, lie):
        lie = lie - 3 # very friendly, decrease age even more :-)
        return method_to_decorate(self, lie)
    return wrapper


class Lucy(object):
    def __init__(self):
        self.age = 32

    @method_friendly_decorator
    def sayYourAge(self, lie):
        print( 'I am {0}, what did you think?'.format(self.age + lie))

l = Lucy()
l.sayYourAge(-3)


"""If you’re making general-purpose decorator--one you’ll apply to any function or method, no matter its arguments--
then just use *args, **kwargs:"""

def a_decorator_passing_arbitrary_arguments(function_to_decorate):
    # The wrapper accepts any arguments
    def a_wrapper_accepting_arbitrary_arguments(*args, **kwargs):
        print( 'Do I have args?:')
        print( args)
        print( kwargs)
        # Then you unpack the arguments, here *args, **kwargs
        # If you are not familiar with unpacking, check:
        # http://www.saltycrane.com/blog/2008/01/how-to-use-args-and-kwargs-in-python/
        function_to_decorate(*args, **kwargs)
    return a_wrapper_accepting_arbitrary_arguments

@a_decorator_passing_arbitrary_arguments
def function_with_no_argument():
    print( 'Python is cool, no argument here.')

function_with_no_argument()

@a_decorator_passing_arbitrary_arguments
def function_with_arguments(a, b, c):
    print( a, b, c)

kwargs = {"arg1" : "Geeks", "arg2" : "for", "arg3" : "Geeks"}
function_with_arguments(1,2, kwargs)

@a_decorator_passing_arbitrary_arguments
def function_with_named_arguments(a, b, c, platypus='Why not ?'):
    print( 'Do {0}, {1} and {2} like platypus? {3}'.format(    a, b, c, platypus))

function_with_named_arguments('Bill', 'Linus', 'Steve', platypus='Indeed!')

#
#-----------------------------------------------------------------------------------------------------------------------
#
def a_decorator_passing_arbitrary_arguments(function_to_decorate):
    # The wrapper accepts any arguments
    def a_wrapper_accepting_arbitrary_arguments(*args, **kwargs):
        print( 'Do I have args?:')
        print( args)
        print( kwargs)
        # Then you unpack the arguments, here *args, **kwargs
        # If you are not familiar with unpacking, check:
        # http://www.saltycrane.com/blog/2008/01/how-to-use-args-and-kwargs-in-python/
        function_to_decorate(*args, **kwargs)
    return a_wrapper_accepting_arbitrary_arguments


class Mary(object):
    def __init__(self):
        self.age = 31

    @a_decorator_passing_arbitrary_arguments
    def sayYourAge(self, lie=-3): # You can now add a default value
        print( 'I am {0}, what did you think?'.format(self.age + lie))

m = Mary()
m.sayYourAge(-3)
