# https://gist.github.com/Zearin/2f40b7b9cfc51132851a

# To understand decorators, you must first understand that functions are objects in Python. This has important consequences. Let’s see why with a simple example :

def shout(word='yes'):
    return word.capitalize() + '!'

print(shout())
print(shout('me'))

# As an object, you can assign the function to a variable like any
# other object

scream = shout

 # Notice we don’t use parentheses: we are not calling the function, we are
# putting the function `shout` into the variable `scream`.
# It means you can then call `shout` from `scream`:

print( scream('yes, me'))

# More than that, it means you can remove the old name `shout`, and
# the function will still be accessible from `scream`

del shout
# shout()
print( scream())


# Another interesting property of Python functions is they can be defined... inside another function!
def talk():

    # You can define a function on the fly in `talk` ...
    def whisper(word='yes'):
        return word.lower() + '...'

    # ... and use it right away!

    print( whisper())

# You call `talk`, that defines `whisper` EVERY TIME you call it, then
# `whisper` is called in `talk`.
talk()

# But `whisper` DOES NOT EXIST outside `talk`:
whisper()


""" Functions references
You’ve seen that functions are objects. Therefore, functions:

    - can be assigned to a variable
    - can be defined in another function
That means that a function can return another function"""

def getTalk(kind='shout'):

    # We define functions on the fly
    def shout(word='yes'):
        return word.capitalize() + '!'

    def whisper(word='yes'):
        return word.lower() + '...'

    # Then we return one of them
    if kind == 'shout':
        # We don’t use '()'. We are not calling the function;
        # instead, we’re returning the function object
        return shout
    else:
        return whisper

getTalk()
getTalk('shout')
getTalk('whisper')()

# How do you use this strange beast?
# Get the function and assign it to a variable
talk = getTalk()
print(talk)
print(talk())

"""But wait...there’s more!
If you can return a function, you can pass one as a parameter: """
def doSomethingBefore(func):
    print('I do something before then I call the function you gave me')
    print( func())

doSomethingBefore(scream)
#outputs:
#I do something before then I call the function you gave me
#Yes!
