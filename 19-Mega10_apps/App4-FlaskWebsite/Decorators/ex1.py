# Decorators made easy
# https://www.youtube.com/watch?v=MYAEv3JoenI
# NOTE: TO FIX

def check_wrapping(item):
    def inside(a,b):
        if b == 0:
            return 'Can\'t divide by 0'
        # else:
        #     item(a,b)
    return inside

@check_wrapping
def div(a,b):
    return a / b

# One way to execute a Decorator
# div = check_wrapping(div)

# print(div(10,0))
# print(div(10,2))
print( div(12,6) )

# print(div.__name__)
