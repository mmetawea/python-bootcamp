"""Fill in the Line class methods to accept coordinates as a pair of tuples and return the slope and distance of the line."""

class Line:

    def __init__(self,p1,p2):
        self.p1 = p1
        self.p2 = p2

    def distance(self):
        dist = ( ((p2[0]-p1[0])**2)+ ( p2[1]-p1[1])**2  )**1/2
        return dist

    def slope(self):
        slope = (p2[1]-p1[1]) / (p2[0]-p1[0])
        return slope

points = Line((-2,5), (3,13))
points.slope()
points.distance()


class Cylinder:

    pi = (22/7)

    def __init__(self,height=1,radius=1):
        self.height=height
        self.radius=radius

    def volume(self):
        #V =  π r2 h
        vol = self.pi * (self.radius**2) * self.height
        return vol

    def surface_area(self):
        # A = 2 π r h  +  2 π r2
        surfarea = ( 2 * self.pi * self.radius * self.height) + ( 2* self.pi * (self.radius**2))
        return surfarea


a = Cylinder()
a.surface_area()
a.volume()
