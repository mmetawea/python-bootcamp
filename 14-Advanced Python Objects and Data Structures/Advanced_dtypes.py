"""Advanced Numbers"""
2**4

pow(2,4)
pow(2,4, z) #2^4 mod z

abs(-1)

round(3.1412)
round(3.1412, 2)

import math
math.__doc__

"""Advanced Strings"""

s = 'hello world'
s.capitalize()
s.title()

s.count('o')
s.find('o')

s.center(20,'z')

'hello\tworld'.expandtabs()

# only looks for first instance
s.partition(" ")
('This is my email: mo@gmail.com'.split(' ')).partition("@")



"""Advanced Sets"""
s = set()
s.add(1)
s.add(3)
s.add(5)
s2 = set({2,4,6})
s
s2

sc = s.copy()
sc.add(4)

sc.difference(s)
s.difference_update(s2)

s1 = {1,2,3}
type(s1)
s2 = {1,4,5}
s1.difference(s2)
s1.difference_update(s2)
s.clear

s1.discard(4)

s1.intersection(s2)
s1.intersection_update(s2)

s1
s4 = s2
s3 = {5}

s1.isdisjoint(s3)
s2.issubset(s4)
s2.issuperset(s4)

s1.symmetric_difference(s2)
s2.symmetric_difference(s1)
s1
s2
s1.union(s2)
s1.update(s2)


"""Advanced Dictionaries"""
d = {'k1':1,'k2':2}

# dict comprehension
dict = {x:x**2 for x in range(10)}

# dict comprehension
{x:x**2 for x in range(10)}
{k:v**2 for k,v in zip(['a', 'b'], range(3,5))}



d = {'k1':1,'k2':2}
type(d)

for k in d.items():
    print (k)
for k in d.keys():
    print (k)
for k in d.values():
    print (k)

d.items()
d.keys()
d.values()


"""Advanced lists """

l = [1,2,3]

l.append(4)
l.count(3)
len(l)

#append: Appends object at end
x = [1, 2, 3]
x.append((4, 5))
x.append([4, 5])
x.append(list(zip([4, 5])))
print (x)

# extend: extends list by appending elements from the iterable
x = [1, 2, 3]
x.extend([4, 5])
print (x)

x.extend([x for x in range(4,10)])
x.sort()
x = set(x)

x.index(9)
x
x.insert(2,'inserted')
x.pop(2)
x.remove('inserted')

x.sort()
x.reverse()
