""" With Statement Context Managers

When you open a file using f = open('test.txt'), the file stays open until you specifically call f.close(). Should an exception be raised while working with the file, it remains open. This can lead to vulnerabilities in your code, and inefficient use of resources.

A context manager handles the opening and closing of resources, and provides a built-in try/finally block should any exceptions occur."""

# Protect the file with try/except/finally
p = open('oops.txt','a')
try:
    p.readlines()
except:
    print('An exception was raised!')
finally:
    p.close()

# p.write('add more text')

with open('oops.txt','r') as p:
    p.readlines()
p.write('which is always equal to the length of the string')
