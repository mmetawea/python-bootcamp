"""Collatz Conjecture - Start with a number n > 1. Find the number of steps it takes to reach one using the following process: If n is even, divide it by 2. If n is odd, multiply it by 3 and add 1. """


counter = 0
def collatz(x):

    def oddNum(x):
        x = (3*x) + 1
        # print(x, ' is odd!')
        if x == 1:
            return print(' Collatz achived! and in {} steps'.format(counter))
        else:
            collatz(x)


    def evenNum(x):
        x = x/2
        # print(x, ' is even! and of type', type(x))

        if x == 1:
            return print(' Collatz achived! and in {} steps'.format(counter))
        else:
            collatz(x)

    # print('The number is ',x, ' and of type', type(x))
    x = int(x)
    global counter
    counter += 1

    if((x&1) == 0):
        evenNum(x)
    else:
        oddNum(x)


collatz(1400)
