"""Multiple child widgets in same parent
A Frame example """

import pythotk as tk

# Create a window
window = tk.Window("Building a frame")

# Create a Frame!
big_frame = tk.Frame(window)
big_frame.pack(fill='both', expand=True)
# big_frame.pack(fill='y', expand=True)

# This defines the status bar
status_bar = tk.Label(window, "Ready!")
status_bar.pack(fill='x')
# status_bar.pack(fill='y')
# status_bar.pack(fill='both')

# Window size defination setup and run
window.geometry(300, 200)
tk.run()
