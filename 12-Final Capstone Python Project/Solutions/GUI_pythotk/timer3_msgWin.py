#!/usr/bin/env python
import tkinter as tk
from tkinter import messagebox as tkMessageBox

def start_timer() :
    root.after(scale.get() * 1000, show_alert)

def show_alert() :
    root.bell()
    # tkMessageBox.showinfo("Ready!", "DING DING DING!")
    messageWindow()
    quit()

def messageWindow() :
    win = tk.Toplevel()
    b = tk.Button(win, text='DING DING DING',
               # bg="blue", fg="yellow",
               activebackground="red", activeforeground="white",
               command=quit)
    b.pack(ipadx=root.winfo_screenwidth(50)/2,
           ipady=root.winfo_screenheight(60)/2)

    root.mainloop()

root = tk.Tk()

# minutes = tk.Label(root, text="Minutes:")
seconds = tk.Label(root, text="Seconds: ")

seconds.grid(row=0, column=0)

scale = tk.Scale(root, from_=1, to=45, orient='horizontal', length=300)
# scale.pack()
scale.grid(row=0, column=1)

btnTimer = tk.Button(root, text="Start timing", command=start_timer)
# button.pack(side="left")
btnTimer.grid(row=1, column=0, pady=5, sticky='E')

btnQuit = tk.Button(root, text="Quit", command=quit)
btnQuit.grid(row = 1, column = 1, pady=12, sticky='E')


label = tk.Label(root)
label.place(x=50, y=15)







# button.pack()
root.mainloop()
