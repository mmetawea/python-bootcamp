import pythotk as tk
import threading
import time

text = tk.Text(tk.Window("Thread Demo"))
text.pack()

def thread_target():
    text.insert(text.end, 'doing things...\n')
    time.sleep(1)
    text.insert(text.end, 'doing more things...\n')
    time.sleep(2)
    text.insert(text.end, 'done')

tk.init_threads()
threading.Thread(target=thread_target).start()
tk.run()
