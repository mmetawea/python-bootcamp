import pythotk as tk

# This creates a Window widget with title "Hello World". A widget is an element of the GUI.
window = tk.Window("Hello World")

# Many widgets need to go into another widget. Label is a widget that displays text, and this line of code puts it in our window. The widget that the label goes in is called the parent or the master widget of the label. Similarly, the label is said to be a child or slave of the window.
label = tk.Label(window, "Hello World!")
# label.config['text'] = 'new text'

# If you create a label into the window, it isn’t displayed automatically. This line of code displays it.
# Creating a child widget and displaying it in the parent are two separate things because this way you can choose how the widget shows up.

def on_click():
    print("You clicked me!")

button = tk.Button(window, "Click me", command=on_click)



# packs the window elements prior to running..
button.pack()
label.pack()

# The code before this runs for just a fraction of a second, but this line of code stays running until we close the window. That’s usually something between a few seconds and a few hours.
tk.run()
