import math

"""
Find e to the Nth Digit - Just like the previous problem, but with e instead of PI. Enter a number and have the program generate e up to that many decimal places. Keep a limit to how far the program will go.

log(...)
        log(x, [base=math.e])
        Return the logarithm of x to the given base.

        If the base not specified, returns the natural logarithm (base e) of x. """

## NOTE: Taking the length of string, room for improvement.

def e_nth(x,n):
    a,b = math.modf(math.log(x))
    y = str(a)
    t = float( y[:n+2]) + b
    print(t)
    # return(x)

e_nth(1000000,4)
