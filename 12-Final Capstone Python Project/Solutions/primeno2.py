# Python program to display all the prime numbers within an interval
""" Using two loops """
# change the values of lower and upper for a different result
lower = 1
# upper = 1000

# uncomment the following lines to take input from the user
#lower = int(input("Enter lower range: "))
upper = int(input("Enter upper range: "))

print("Prime numbers between",lower,"and",upper,"are:")

for num in range(lower,upper + 1):
   # prime numbers are greater than 1
   if num > 1:
       for i in range(2,num):
           if (num % i) == 0:
               break
       else:
           print(num)

[x for x in range(10)
    if (x % y != 0 for y in range(2,x)) ]

def getPrimes(z):
    ## Using list Comperhansions
    return [x for x in range(2, z)
        if all(x % y != 0 for y in range(2, x)) ]


def getPrimes(z):
    ## using filter, most awesome
    return list ( filter(lambda x:all(x % y != 0 for y in range(2, x)), range(2, z)) )

getPrimes(20)
