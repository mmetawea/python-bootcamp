class SortClass(object):
    # Sorting class
    def __init__(self, array):
        self.array = array

    def mergeSort(self):
        # Implementation of Merge Sort algo
        if len(self.array) > 1:
            m = len(self.array)//2
            left, right = self.array[:m] , self.array[m:]

            SortClass(left).mergeSort(), SortClass(right).mergeSort()

            i,j,k = 0,0,0

            while i < len(left) and j < len(right):
                if left[i] < right[j]:
                    self.array[k] = left[i]
                    i += 1
                else:
                    self.array[k] = right[j]
                    j += 1
                k += 1

            while i < len(left):
                self.array[k] = left[i]
                i += 1
                k += 1

            while j < len(right):
                self.array[k] = right[j]
                j += 1
                k += 1
        return self.array

x = SortClass([1,6,5,2,10,8,7,4,3,9])
x.mergeSort()
x.array

print(SortClass([1,6,5,2,10,8,7,4, 4, 7,3,9]).mergeSort())
