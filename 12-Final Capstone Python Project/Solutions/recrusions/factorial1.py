def fact(n):
    if n == 0:
        return 1
    else:
        return n * fact(n-1)


def iterative_factorial(n):
    result = 1
    for i in range(2,n+1):
        result *= i
    return result


print(fact(0))
print(fact(5))
print(iterative_factorial(0))
print(iterative_factorial(5))
