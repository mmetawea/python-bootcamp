""" A Script to find prime numbers
2, 3, 5. 7, 11, 13, 17, 19, .. """

def prime(x):
    """ Check if the number is a prime """
    result = []
    for i in range(2,x):
        if not x % i:
            result.append(i)
            # print(result)
    # print(len(result))
    if not len(result):
        # print ('{} is a prime'.format(x))
        return x

def rtrPrime(x):
    primes = []
    for num in range(2,x):
        if prime(num):
            primes.append( prime(num) )
    return primes

def getPrime(x):
    primes = []
    for num in range(2,x):
        if prime(num):
            primes.append( prime(num) )
    print( primes )

getPrime(10)
