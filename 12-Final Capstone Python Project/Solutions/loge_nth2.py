 ### https://github.com/rlingineni/PythonPractice/blob/master/eCalc/eCalculate.py
import math

from decimal import *

def CalculateE(val, roundVal):
		someE = round(val, roundVal);
		# e = str(someE)
		# someList = list(e)
		return someE;

def piVal(roundVal):
    return CalculateE(Decimal(22/7), roundVal)

def eVal(roundVal):
    return CalculateE(math.e, roundVal)


roundTo = int(input('Enter the number of digits you want after the decimal for e: '))
try:
    roundint = int(roundTo);
    print("Pi rounded to {} is {}".format(roundTo, piVal(roundint)  ));
    print("Natural log rounded to {} is {}".format(roundTo, eVal(roundint)  ));
except:
	print("You did not enter an integer");
