factors = lambda n: [x for x in range(1,n+1) if not n%x]
is_prime = lambda n: len(factors(n))==2
primefactors = lambda n: list(filter(is_prime,factors(n)))

def prime(n):
    x = list(filter(is_prime, factors(n)))
    # y = is_prime(n)
    # print(x)
    return x


prime(100)

is_prime(102)
