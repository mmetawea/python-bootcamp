"""Fibonacci Sequence - Enter a number and have the program generate the Fibonacci sequence to that number or to the Nth number."""

def fibgen(n):
    a,b = 1,1
    for num in range(n):
        yield a
        a,b = b, a+b

for i in range(10):
    print(next(fibgen(i)))

list(fibgen(10))

for i in fibgen(10):
    print(i)
