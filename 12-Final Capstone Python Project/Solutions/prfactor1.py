# a = [10, 20 ,30]
# b = [1,2,3]
# [(x,y) for x in a for y in b]

factors = lambda n: [x for x in range(1,n+1) if not n%x]
is_prime = lambda n: len(factors(n))==2
primefactors = lambda n: list(filter(is_prime,factors(n)))

def primer(y):

    def primeFactorize(n):
        n = int(n)
        f = primefactors(n)
        factors = []
        if is_prime(n):
            return str(n)
        else:
            return str(f[0]) + ", " + primeFactorize(n/f[0])

    x = primeFactorize(y)
    # print(x)
    lst = x.split(',')
    return [int(x) for x in lst]


primer(28)
