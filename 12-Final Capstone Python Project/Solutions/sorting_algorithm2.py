""" Sorting - Implement two types of sorting algorithms: Merge sort and bubble sort."""

class Sort:

    def __init__(self, array):
        self.array = array

    def merge_sort(self):
        """Merge Sort is a Divide and Conquer algorithm. It divides input array in two halves, calls itself for the two halves and then merges the two sorted halves. The merge() function is used for merging two halves """

        # print("Called array: ", self.array)
        if len(self.array) >1:
            # Finding the middle      [ 5, 1, 4, 2, 8 ]
            mid = len(self.array) //2
            # Splitting the arrary  [5,1]  [4,2,8]
            # print("Splitting ")
            L = self.array[:mid]
            R = self.array[mid:]
            # print("\tLeft Handside ",L)
            # print("\tRight Handside ",R)
            # Sorting the splits
            Sort(L).merge_sort()
            Sort(R).merge_sort()

        # Define counters..
            i = j = k = 0

            while i < len(L) and j < len(R):
                if L[i] < R[j]:
                    self.array[k] = L[i]
                    i += 1
                else:
                    self.array[k] = R[j]
                    j += 1
                k += 1

             # Checking if any element was left
            while i < len(L):
                self.array[k] = L[i]
                i += 1
                k += 1

            while j < len(R):
                self.array[k] = R[j]
                j += 1
                k += 1
            # print('Merging ', self.array)

    def bubble_sort(self):
        """ Bubble Sort is the simplest sorting algorithm that works by repeatedly swapping the adjacent elements if they are in wrong order. """
        for j in range(len(self.array)):
            for i in range(0,(len(self.array) - j - 1)):
                if self.array[i] > self.array[i+1]:
                    self.array[i], self.array[i+1] = self.array[i+1], self.array[i]

        return self.array


if __name__ == '__main__':
    arr = [12, 11, 13, 5, 6, 7, 1, 2, 4 ,9]
    print ("Given array is", end="\t\t\t\t")
    print(arr)
    print("Sorted array with Bubble is: ", end="\t")
    Sort(arr).bubble_sort()
    print(arr)
    arr = [12, 11, 13, 5, 6, 7, 1, 2, 4 ,9]
    Sort(arr).merge_sort()
    print("Sorted array with Merge is: ", end="\t")
    print(arr)
