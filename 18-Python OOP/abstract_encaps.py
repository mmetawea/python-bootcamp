"""Library management system..
The first task is the customer should be able to view all the books in the library.
The second task is the process should be handled when a customer requests to borrow a book and the final
task is the library collection should be updated when the customer returns a book."""

class Library:

    def __init__(self):

        self.libraryBooks = { 'Python for beginners': True, 'Programming for dummies': True, 'Your first steps': True}
        # self.bookTitle = title


    def viewBooks(self):
        # return self.libraryBooks
        print('Available books: ')
        for book in self.libraryBooks:
            print(book)


    def borrowBook(self, title):
        if self.libraryBooks[title]:
            self.libraryBooks[title] = False
            print('Book %s is now borrowed..' %title)
        else:
            print('Unfortuently this book is not available!')

    def returnBook(self, title):
        if not self.libraryBooks[title]:
            self.libraryBooks[title] = True
            print('Thank you for returning %s.' %title)
        else:
            print('This book is not ours :)')

    def addBook(self, title):

        if title in self.libraryBooks:
            if self.libraryBooks[title]:
                print("The book title'", k, "' exists and currently available." )
            else:
                print("The book title'", k, "' exists and currently on loan." )
        else:
            self.libraryBooks[title] = True
            print('Thank you for your donation')


class Customer:
    def requestBook(self):
        print('Enter the name of a book you would like to borrow: ')
        self.book = input()
        return self.book

    def returnBook(sef):
        print('Enter the name of a book you are returning: ')
        self.book = input()
        return self.book

AlexLib =  Library()

AlexLib.viewBooks()
AlexLib.borrowBook('Python for beginners')
AlexLib.returnBook('Python for beginners')
AlexLib.addBook('Your Second step')
