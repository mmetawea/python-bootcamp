"""Polymorphism
- The ability of an entity to be able to exist in more than one form

* Overriding:
- redifining the behaviour of a base class method in a derived class

* Overloading:
defining a special method of an operator within your class to handle the operation between the objects of that class.

* Abstract Base Class:
a base class which consisits of abstract methods that should be implemented in its derived class """

from abc import ABCMeta, abstractmethod


class Shape(metaclass=ABCMeta):
    @abstractmethod
    def area(self):
        return 0


class Square(Shape):

    def __init__(self, side):
        self.side = side

    def __add__(SquareA, SquareB):
        return ((SquareA.side * 4) + (SquareB.side * 4))

    def area(self):
        print(f'Area of square: {(self.side)**2}')


class Rectangle(Shape):
    def __init__(self, width, length):
        self.width = width
        self.length = length

    def area(self):
        print(f'Area of rectangle: {(self.width * self.length )}')


square = Square(4)
rectangle = Rectangle(4, 6)
square.area()
rectangle.area()

# SqA = Square(4)
# SqB = Square(8)
# print(SqA+SqB)
