"""Banking System

You first need to provide a prompt to the user asking if they wish to create a new savings account or access an existing one.

And if it is a new savings account that they wish to create, you need to accept their name and initial deposit
And then in your program you need to have a five digit random number generator.
And this random number will be the account number of their new savings account.

And if it is an existing account that they wish to access you need to accept their name and account number
and check if it's a valid user.
And then you need to provide them options to withdraw, deposit or display their available balance if it is a valid user.
Now before you move on to the next lecture where we will implement a solution for this problem."""
#!Python
import random

from abc import ABCMeta, abstractmethod


class Account(metaclass=ABCMeta):
    @abstractmethod
    def createAccount():
        return 0

    def authenticate():
        return 0

    def withDraw():
        return 0

    def deposit():
        return 0

    def displayBalance():
        return 0


class SavingsAccount:

    def __init__(self):
        # Initiate Banking database
        # self.bankDb = { 991287388: 'John Doe', 987127312 : 'Ali J', 9012834651: 'Kirk Douglas'}
        # self.bookTitle = title
        self.savingsAccounts = {}


    def viewAccounts(self):
        return self.savingsAccounts

    def createAccount(self, custName, initialDeposit):
        # [Key][0]=name , [Key][1]= balance

        self.accountNumber = random.randint(10000, 99999)
        self.savingsAccounts[self.accountNumber] = [custName, initialDeposit]
        print('Account creation successful, your account number is ',
              self.accountNumber)

    def authenticate(self, custName, accountNumber):

        if accountNumber in self.savingsAccounts.keys():
            if self.savingsAccounts[accountNumber][0] == custName:
                print('Authentication successful')
                self.accountNumber = accountNumber
                return True
            else:
                print('Authentication Failed')
                return False

    def withDraw(self, withdrawAmount):
        if withdrawAmount > self.savingsAccounts[self.accountNumber][1]:
            print('Insufficient balance')
        else:
            self.savingsAccounts[self.accountNumber][1] -= withdrawAmount
            print('Withdraw successful, ', self.displayBalance())

    def deposit(self, depositAmount):
        self.savingsAccounts[self.accountNumber][1] += depositAmount
        print('Deposit was successful, ', self.displayBalance())

    def displayBalance(self):
        print('Available balance: ',
              self.savingsAccounts[self.accountNumber][1])



savingsAccount = SavingsAccount()

print(" Good Day\n", "How can we help today?")
while True:
    try:
        print("Input 1 for creating a new saving account, or 2 for accessing an existing one: ")
        userInput = int(input('You can press 3 to quit\n'))
        if userInput == 1:
            custName = input('Please enter your name: ')
            initialDeposit = int(input('Please enter the initial deposit amount: '))
            savingsAccount.createAccount(custName, initialDeposit)
            # break
        elif userInput == 2:
            custName = input('Please enter your name: ')
            accountNumber = int(input('Please enter your account number: '))
            if savingsAccount.authenticate(custName, accountNumber):
                savingsAccount.displayBalance
                while True:
                    print('Enter 1 to withdraw')
                    print('Enter 2 to deposit')
                    print('Enter 3 to display available balance')
                    print('Enter 4 to return to the previous menu')
                    userInput = int(input())
                    if userInput is 1:
                        print('Enter an amount to withdraw')
                        withdrawAmount = int(input())
                        savingsAccount.withDraw(withdrawAmount)
                    elif userInput is 2:
                        print('Enter an amount to deposit')
                        depositAmount = int(input())
                        savingsAccount.deposit(depositAmount)
                    elif userInput is 3:
                        savingsAccount.displayBalance()
                    elif userInput is 4:
                        break
        elif userInput == 3:
            exit()
        else:
            print('Invalid input, please try again.')
            continue
    except Exception as e:
        print('Invalid input, please try again.')
        continue
