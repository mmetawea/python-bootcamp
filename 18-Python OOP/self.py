class Employee:
    def emplyeeDetails(self):
        self.name = 'Matthew'
        self.age = 30
        print('Name = %s' %self.name)
        print('Age = %s' %self.age)

    def printEmployeeDetails(self):
        print('Printing in another method')
        print('Name = %s' %self.name)
        print('Age = %s' %self.age)

employee = Employee()
employee.emplyeeDetails()
# Employee.emplyeeDetails(employee)

employee.printEmployeeDetails()
