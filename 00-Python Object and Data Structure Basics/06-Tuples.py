"""# Tuples"""
#In Python tuples are very similar to lists, however, unlike lists they are *immutable* meaning they can not be changed. You would use tuples to present things that shouldn't be changed, such as days of the week, or dates on a calendar. \n", '\n', 'In this section, we will get a brief overview of the following:\n', '\n', '    1.) Constructing Tuples\n', '    2.) Basic Tuple Methods\n', '    3.) Immutability\n', '    4.) When to Use Tuples\n', '\n', "You'll have an intuition of how to use tuples based on what you've learned about lists. We can treat them very similarly with the major distinction being that tuples are immutable.\n", '\n', '## Constructing Tuples\n', '\n', 'The construction of a tuples use () with elements separated by commas. For example:']

# Create a tuple
t = (1,2,3)
mylist = [1,2,3]

# Check len just like a list
len(t)

type(t)


# Can also mix object types
t = ('one',2)

# Use indexing just like we did in lists
t[0]
t[:]

# Slicing just like a list\n', 't[-1]']
t[-1]



## Basic Tuple Methods
#"Tuples have built-in methods, but not as many as lists do. Let's look at two of them:
t = ('a', 'b', 'b')
# Use .index to enter a value and return the index
t.index('a')
# Use .count to count the number of times a value appears
t.count('b')

## Immutability
#It can't be stressed enough that tuples are immutable. To drive that point home:
t[0]= 'change'

#Because of this immutability, tuples can't grow. Once a tuple is made we can not add to it.
t.append('nope')


t = (1,2, [1,2])
type(t)
t[2][1]
## When to use Tuples
#You may be wondering, "Why bother using tuples when they have fewer available methods?" To be honest, tuples are not used as often as lists in programming, but are used when immutability is necessary. If in your program you are passing around an object and need to make sure it does not get changed, then a tuple becomes your solution. It provides a convenient source of data integrity.
#You should now be able to create and use tuples in your programming as well as have an understanding of their immutability.

#Up next Sets and Booleans!!

## When to use Tuples, You may be wondering, "Why bother using tuples when they have fewer available methods?" To be honest,
#tuples are not used as often as lists in programming, but are used when immutability is necessary.
#If in your program you are passing around an object and need to make sure it does not get changed, then a tuple becomes your solution.
#It provides a convenient source of data integrity.'You should now be able to create and use tuples in your programming as well as have an understanding of their immutability.\n', '\n', 'Up next Sets and Booleans!!']
