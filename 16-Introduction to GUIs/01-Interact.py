"""Using Interact
The interact function (ipywidgets.interact) automatically creates user interface (UI) controls for exploring code and data interactively. It is the easiest way to get started using IPython’s widgets"""
# Start with some imports!

from ipywidgets import interact, interactive, fixed
import ipywidgets as widgets

def f(x):
    return x

interact(f, x = 10);
interact(f, x=True);
interact(f, x='Hi there!');

# Using a decorator!
@interact(x=True, y=1.0)
def g(x, y):
    return (x, y)

"""Fixing arguments using fixed"""
# There are times when you may want to explore a function using interact, but fix one or more of its arguments to specific values. This can be accomplished by wrapping values with the fixed function.

# Again, a simple function
def h(p, q):
    return (p, q)

interact(h, p=5, q=fixed(20));

# Can call the IntSlider to get more specific
interact(f, x=widgets.IntSlider(min=-10,max=30,step=1,value=10));

# Min,Max slider with Tuples
interact(f, x=(0,4));

# (min, max, step)
interact(f, x=(0,8,2));


def f(x:True): # python 3 only
    return x

f(True)
