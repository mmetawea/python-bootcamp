"""Built-in Functions Test"""

# Problem 1

def word_lengths(phrase):
    # Use map() to create a function which finds the length of each word in the phrase (broken by spaces) and returns the values in a list.
    # The function will have an input of a string, and output a list of integers.
    wordlst= phrase.split(" ")
    x = list(map(lambda x: len(x), wordlst))
    return x

def word_lengths(phrase):
    # Use map() to create a function which finds the length of each word in the phrase (broken by spaces) and returns the values in a list.
    # The function will have an input of a string, and output a list of integers.

    return list(map(lambda x: len(x), phrase.split(" ")))


word_lengths('How long are the words in this phrase')


# Problem 2

from functools import reduce

def digits_to_num(digits):
    # Use reduce() to take a list of digits and return the number that they correspond to. For example, [1, 2, 3] corresponds to one-hundred-twenty-three.

    # Do not convert the integers to strings! - FAIL
    # return reduce(lambda a,b: str(a)+str(b), digits)
    # using 10s multiplications
    return reduce( lambda x,y: x*10 + y, digits)

digits_to_num([3,4,3,2,1])



# Problem 3


def filter_words(word_list, letter):
    # Use filter to return the words from a list of words which start with a target letter.

    return list( filter(lambda x: x[0]==letter , word_list)) #returns odd


l = ['hello','are','cat','dog','ham','hi','go','to','heart']
filter_words(l,'h')

def filterString(string, letter):
    # Use filter to return the words from a string which start with a target letter.
    # lst = string.split(' ')
    # returns only first letter
    return list( filter(lambda x: (x[0]==letter) , string)) #returns odd

l2 = 'hello, Are you a cat dog ham hi go to heart'
l2 = l2.lower().split(' ')
filter_words(l2,'h')



# Problem 4


def concatenate(L1, L2, connector):
    # Use zip() and a list comprehension to return a list of the same length where each value is the two strings from L1 and L2 concatenated together with connector between them. Look at the example output below:
    zipped = []
    zipped = [(a,connector,b) for a,b in (zip(L1,L2))]
    return [a+connector+b for a,b in (zip(L1,L2))]

concatenate(['A','B'],['a','b'],'-')

def concatenate(L1, L2, connector):
    return [word1+connector+word2 for (word1,word2) in zip(L1,L2)]


Problem 5

    # Use enumerate() and other skills to return a dictionary which has the values of the list as keys and the index as the value. You may assume that a value will only appear once in the given list.
    def d_list(L):
    dout={}
    for i,j in enumerate(L):
        dout[j]=i
    # return dout

def d_list(L):
    return {key:value for value,key in enumerate(L)}
d_list(['a','b','c'])


# Problem 6


def count_match_index(L):
    # Use enumerate() and other skills from above to return the count of the number of items in the list whose value equals its index.
    return len([key for value,key in enumerate(L) if value == key])


count_match_index([0,2,2,1,5,5,6,10])
