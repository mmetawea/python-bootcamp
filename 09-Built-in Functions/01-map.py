"""map() is a built-in Python function that takes in two or more arguments: a function and one or more iterables, in the form:

map(function, iterable, ...) """

def fahrenheit(celsius):
    return (9/5)*celsius + 32

temps = [0, 22.5, 40, 100]

F_temps = map(fahrenheit, temps)
F_temps # Map is not callable..
list(F_temps)

## Using lambda
list( map( lambda x: (9/5)* x +32, temps) )

"""map() with multiple iterables
map() can accept more than one iterable. The iterables should be the same length - in the event that they are not, map() will stop as soon as the shortest iterable is exhausted. """

a = [1,2,3,4]
b = [5,6,7,8]
c = [9,10,11,12]

list( map( lambda x,y: x+y, a,b))
list( map( lambda x,y: x+y, a,c))
