"""filter

The function filter(function, list) offers a convenient way to filter out all the elements of an iterable, for which the function returns True.

The function filter(function,list) needs a function as its first argument. The function needs to return a Boolean value (either True or False). This function will be applied to every element of the iterable. Only if the function returns True will the element of the iterable be included in the result.

Like map(), filter() returns an iterator - that is, filter yields one result at a time as needed. Iterators and generators will be covered in an upcoming lecture. For now, since our examples are so small, we will cast filter() as a list to see our results immediately."""

def even_check(num):
    # Checking even or odd using modulus division
    if num%2 ==0:
        return True

def evenCheck(num):
    ## Checking even or odd using bit operations..
    if num&1 ==0:
        return True
    else:
        return False

even_check(23)
evenCheck(22)

lst =range(20)

filter(None, lst) #creates an object, doesn't call
list(filter(evenCheck, lst)) # calls and return a list

#using lambda
list( filter(lambda x: x&1 , lst)) #returns odd
list( filter(lambda x: ~x&1 , lst)) #returns even
list( filter(lambda x: (x&1 ==1 ), lst)) #returns odd
list( filter(lambda x: (x&1 ==0 ), lst)) #returns even
